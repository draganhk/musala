*** Settings ***
Documentation     Test Case 1
Suite Setup       Setup Test
Suite Teardown    Teardown Action
Resource          resource.robot

*** Test Cases ***
Test Case 1
    [Tags]    SiteTestovi    Test1
    ${EMAIL FILE} =    Get File    ./test case 1 pogresni majlovi/emails.csv
    ${EMAIL FILE} =    Split To Lines    ${EMAIL FILE}
    #${EMAIL FILE} =    Split String    ${EMAIL FILE}    /n
    Remove From List    ${EMAIL FILE}    0
    ${EMAILS LENGTH} =    Get Length    ${EMAIL FILE}    #treba length da e 5
    Execute JavaScript    window.scrollTo(0,900)    #Scroll Element Into View    //span[@data-alt="Contact us"]
    Click Element    //span[@data-alt="Contact us"]    #Click Button    CONTACT US
    ${KF_STATUS} =    Run Keyword and Return Status    Wait Until Element Is Visible    //div[@id="fancybox-content"]    ${LOAD_BREAK}    Kontakt formata ne se prikaza posle ${LOAD_BREAK}
    FOR    ${INDEX}    IN RANGE    0    ${EMAILS LENGTH}
        Input Text    //input[@name="your-name"]    Dragan
        ${Email} =    Get From List    ${EMAIL FILE}    ${INDEX}
        Input Text    //input[@name="your-email"]    ${Email}
        Input Text    //input[@name="your-subject"]    test
        Input Text    //textarea[@name="your-message"]    Test
        #Click Element    //div[@class="recaptcha-checkbox-border"]
        #${CAPTCHA_STATUS} =    Get Element Attribute    //div[@class="recaptcha-checkbox-border"]    style
        #Run Keyword If    "${CAPTCHA_STATUS}"=="display: none;"    Execute Manual Step
        Click Button    //input[@type="submit"]
        Wait Until Element Is Visible    //div[@class="wpcf7-response-output"]
        ${INVALID EMAIL} =    Get Text    //span[@class="wpcf7-not-valid-tip"]
        Should Be True    "The e-mail address entered is invalid."=="${INVALID EMAIL}"
        Log    ${Email} is an invalid email
    END
