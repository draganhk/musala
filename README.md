# ROBOT Framework Testovi

## Python pip biblioteki so koi imam raboteno prethodno
* robotframework-httplibrary3
* robotframework-seleniumlibrary
* robotframework-csvlibrary-py3
* robotframework-imaplibrary2
* robotframework-faker
* robotframework-ftplibrary
* robotframework-requests
* robotframework-xvfb
* robotframework-archivelibrary 
* pyyaml
* pyvirtualdisplay 
* selenium

**Nivnite keyword dokumentacii:**
* https://robotframework.org/robotframework/
* https://robotframework.org/robotframework/latest/libraries/BuiltIn.html
* https://peritus.github.io/robotframework-httplibrary/HttpLibrary.html
* https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
* https://rawgit.com/s4int/robotframework-CSVLibrary/master/doc/CSVLibrary.html
* https://rickypc.github.io/robotframework-imaplibrary/doc/ImapLibrary.html
* https://guykisel.github.io/robotframework-faker/
* https://kowalpy.github.io/Robot-Framework-FTP-Library/FtpLibrary.html
* http://bulkan.github.io/robotframework-archivelibrary (deprecated)


## Struktura
* Sekoja posebna komponenta od web stranata koja ja testiram bi ja stavil vo **poseben** ili razlicen folder. Taka mnogu lesno mozam pravam grupiranje i logicki sekcii na komponentite
  * Sekoja posebna test komponenta si go povikuva resource.robot fajlot vo sopstveniot folder. Toa sluzi kako edinstveno zaglavje
  * Sekoj resource.robot fajl kako zaglavje pak go poviguva resource.robot fajlot vo folderot nad nego. Na kraj, main_resource.robot gi povikuva sistemskite python biblioteki od sistemot
* Dopolnitelno, dodavam tagovi na site testovi za da mozat posebni testovi da se pushtaat individualno ili drugi da se ignoriraat po potreba
* Sekoj test si ima svoj "Setup" i "Teardown". So ova, osiguruvam deka testovite sekogas ke gi odrabotat ovie "keywords" funkcii kako prva i posledna 
  * Setup sekogas ke bide zadolzen za otvaranje na prebaruvac ili browser, i odenje to posakuvanata URL lokacija na komponentata
  * Teardown sekogas ke ja gasi aktivnata browser sesija
* Vo ovoj, isklucitelen i mal slucaj, site testovi stojat ramo do ramo vo isti folder i verojatno ne se drzat tolku do gore-spomenatata struktura


## Startuvanje
```
start.sh
```

### Rezultati
Rezultatite, soodvetno na izbranite opcii vo start.sh, ke bidat generirani vo /results/ folderot pod format ./Prebaruvach_browser-2022-03-19-13-34-55-986681/ . Soodvetno tamu gi ima 3te standardni fajlovi od output
