*** Settings ***
Documentation     Glaven resource fajl
Library           SeleniumLibrary    implicit_wait=2    timeout=2    run_on_failure=Nothing
Library           OperatingSystem
Library           String
Library           Collections
Library           DateTime
Library           FakerLibrary
Library           Screenshot
Library           Dialogs

*** Variables ***
${SMALL_SLEEP}    0.5s
${DELAY}          1s
${DEFAULT_SLEEP}    2s
${ALERT_SLEEP}    3s
${LOAD_BREAK}     5s
${EXTENDED_TIMEOUT}    10s
${HEAVY_TIMEOUT}    20s
#-----variables from test case 3 below
${CAREERS LINK}    //ul[@id="menu-main-nav-1"]/li[5]/a
${JOIN US LINK URL VERIFICATION}    /careers/join-us/
${SELECT OPTION VERIFICATION}    Anywhere

*** Keywords ***
Setup Test
    Log    Start Here
    Open Browser Session

Open Browser Session
    Open Browser    ${URL}    ${BROWSER}
    Set Window Size    1440    1024

Teardown Action
    Log    End Here
    Close All Browsers

Wait For Page Reload
    [Arguments]    ${ELEMENT}    ${WAIT UNLOAD}=${LOAD_BREAK}    ${WAIT LOAD}=${EXTENDED_TIMEOUT}
    ${STATUS} =    Run Keyword and Return Status    Wait Until Element Is Not Visible    ${ELEMENT}    ${WAIT UNLOAD}    Stranata ne se izgasi posle ${WAIT UNLOAD}
    Wait Until Element Is Visible    ${ELEMENT}    ${WAIT LOAD}    Stranata ne se vcita posle ${WAIT LOAD}
    Sleep    ${DEFAULT SLEEP}

Test Alert
    [Arguments]    ${ALERT}    ${ELEMENT}
    ${TEXT} =    Get Text    ${ALERT}
    Log    ${ELEMENT} has error: ${TEXT}

Setup Careers Page
    [Arguments]    ${PLACE}=${SELECT OPTION VERIFICATION}
    Execute JavaScript    window.scrollTo(0,0)
    Run Keyword If    "${BROWSER}"=="chrome"    Click Element    ${CAREERS LINK}
    ${FF LINK} =    Run Keyword If    "${BROWSER}"=="firefox"    Get Element Attribute    ${CAREERS LINK}    href
    Run Keyword If    "${BROWSER}"=="firefox"    Go To    ${FF LINK}
    Click Button    Check our open positions
    Wait Until Location Contains    ${JOIN US LINK URL VERIFICATION}
    #------------
    ${SELECT VALUES} =    Get List Items    //select[@name="get_location"]
    List Should Contain Value    ${SELECT VALUES}    ${PLACE}
    Select From List By Value    //select[@name="get_location"]    ${PLACE}