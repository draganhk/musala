*** Settings ***
Documentation     Test Case 4
Suite Setup       Setup Test
Suite Teardown    Teardown Action
Resource          resource.robot

*** Test Cases ***
Test Case 4
    [Tags]    SiteTestovi    Test4
    Setup Careers Page    Sofia
    Log To Console    Sofia
    Get Box Info
    #------------
    Setup Careers Page    Skopje
    Log To Console    Skopje
    Get Box Info

*** Keywords ***
Get Box Info
    ${SHORTER} =    Set Variable    //div[@class="inner-wraper"]/article
    ${HOW MANY} =    Get Element Count    ${SHORTER}
    FOR    ${INDEX}    IN RANGE    1    ${HOW MANY}+1
        ${SHORT} =    Catenate    ${SHORTER} [${INDEX}]/div/a/div/div/h2
        ${POSITION} =    Get Text    ${SHORT}
        ${LINK SHORT} =    Catenate    ${SHORTER} [${INDEX}]/div/a
        ${LINK} =    Get Element Attribute    ${LINK SHORT}    href
        Log To Console    Position: ${POSITION}
        Log To Console    More info: ${LINK}
    END
