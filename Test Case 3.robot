*** Settings ***
Documentation     Test Case 3
Suite Setup       Setup Test
Suite Teardown    Teardown Action
Resource          resource.robot

*** Variables ***
${QA LINK URL VERIFICATION}    /job/experienced-automation-qa-engineer/
@{SECTIONS}       General Description    Requirements    Responsibilities    What we offer

*** Test Cases ***
Test Case 3
    [Tags]    SiteTestovi    Test3
    Test3

*** Keywords ***
Test3
    Setup Careers Page
    #Wait For Page Reload    //select[@name="get_location"]
    Check Box    Experienced Automation QA Engineer
    Wait Until Location Contains    ${QA LINK URL VERIFICATION}
    #------------
    Verify Main Sections
    Execute JavaScript    window.scrollTo(0,900)
    Page Should Contain Element    //input[@value="Apply"]
    Click Element    //div[@class="btn-apply-container"]/a/input
    Wait Until Element Is Visible    //div[@id="fancybox-content"]    ${LOAD_BREAK}    Aplikaciskata forma ne se prikaza posle ${LOAD_BREAK}
    #------------
    Input Text    //input[@name="your-email"]    Dragan@Hadji-Kotarov
    Input Text    //input[@name="linkedin"]    https://www.linkedin.com/in/draganhadjikotarov/
    ${FILENAME} =    Get CV File    cv
    Choose File    //input[@name="uploadtextfield"]    ${FILENAME}
    Click Element    //label[@for="adConsentChx"]
    Click Button    //input[@type="submit"]
    Wait Until Element Is Visible    //div[@class="message-form-content"]
    Sleep    ${DEFAULT_SLEEP}
    Click Element    //div[@class="message-form-content"]/button
    Test For Errors

Check Box
    [Arguments]    ${TEXT}=SomethingRandom
    ${SHORTER} =    Set Variable    //div[@class="inner-wraper"]/article
    ${HOW MANY} =    Get Element Count    ${SHORTER}
    FOR    ${INDEX}    IN RANGE    1    ${HOW MANY}+1
        ${SHORT} =    Catenate    ${SHORTER}    [${INDEX}]/div/a/div/div/h2
        ${LINK SHORT} =    Catenate    ${SHORTER}    [${INDEX}]/div/a
        ${CHECK} =    Get Text    ${SHORT}
        Run keyword If    "${CHECK}"=="${TEXT}"    Click Element    ${LINK SHORT}
        Exit For Loop If    "${CHECK}"=="${TEXT}"
    END

Verify Main Sections
    ${HOW MANY} =    Get Element Count    //div[@class="content-title"]
    Should Be True    ${HOW MANY}==4
    FOR    ${INDEX}    IN RANGE    0    ${HOW MANY}
        ${TITLE} =    Get From List    ${SECTIONS}    ${INDEX}
        ${TITLE} =    Convert To Lower Case    ${TITLE}
        ${INDEXP1} =    Evaluate    ${INDEX}+1
        ${HTML TITLE} =    Get Text    xpath=(//div[@class="content-title"])[${INDEXP1}]
        ${HTML TITLE} =    Convert To Lower Case    ${HTML TITLE}
        Should Be True    "${TITLE}"=="${HTML TITLE}"
    END

Get CV File
    [Arguments]    ${SHOULD CONTAIN}
    ${ARR DIRS} =    List Directories In Directory    ${/}${CURDIR}${/}
    Log    ${ARR DIRS}
    List Should Contain Value    ${ARR DIRS}    ${SHOULD CONTAIN}
    ${ARR FILES} =    List Files In Directory    ${/}${CURDIR}${/}${SHOULD CONTAIN}    *.pdf    absolute
    Log    Working Directory: ${CURDIR}
    Log    Target Directory to get file from: ${SHOULD CONTAIN}
    Log    Current files in Target Directory: ${ARR FILES}
    #------------
    ${FILE NAME} =    Get From List    ${ARR FILES}    0
    Return From Keyword    ${FILE NAME}

Test For Errors
    ${SPAN} =    Set Variable    //span[@class="wpcf7-not-valid-tip"]
    ${INPUTS TOTAL} =    Get Element Count    ${SPAN}
    FOR    ${INDEX}    IN RANGE    1    ${INPUTS TOTAL}+1
        ${ERROR} =    Set Variable    xpath=(${SPAN})[${INDEX}]
        ${PARENT} =    Get Element Attribute    xpath=(${SPAN})[${INDEX}]//parent::span    class
        ${PARENT} =    Fetch From Right    ${PARENT}    ${SPACE}
        ${ERROR EXISTS} =    Run Keyword and Return Status    Page Should Contain Element    ${ERROR}
        Run keyword If    ${ERROR EXISTS}    Test Alert    ${ERROR}    ${PARENT}
    END
