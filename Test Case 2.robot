*** Settings ***
Documentation     Test Case 2
Suite Setup       Setup Test
Suite Teardown    Teardown Action
Resource          resource.robot

*** Variables ***
${COMPANY LINK}    //ul[@id="menu-main-nav-1"]/li/a
${COMPANY LINK URL VERIFICATION}    /company/
${LEADERSHIP SECTION}    //section[@class="company-members"]
${LEADERSHIP SECTION TEXT VERIFICATION}    Leadership
${FACEBOOK ICON}    //span[@class="musala musala-icon-facebook"]
${FACEBOOK URL}    https://www.facebook.com/MusalaSoft?fref=ts

*** Test Cases ***
Test Case 2
    [Tags]    SiteTestovi    Test2
    Execute JavaScript    window.scrollTo(0,0)
    Run Keyword If    "${BROWSER}"=="chrome"    Click Element    ${COMPANY LINK}
    ${FF LINK} =    Run Keyword If    "${BROWSER}"=="firefox"    Get Element Attribute    ${COMPANY LINK}    href
    Run Keyword If    "${BROWSER}"=="firefox"    Go To    ${FF LINK}
    Wait Until Location Contains    ${COMPANY LINK URL VERIFICATION}
    Page Should Contain Element    ${LEADERSHIP SECTION}
    ${SECTION TEXT} =    Get Text    ${LEADERSHIP SECTION}/div/h2
    Should Be True    "${LEADERSHIP SECTION TEXT VERIFICATION}"=="${SECTION TEXT}"
    #------------
    Check For Popup
    Execute JavaScript    window.scrollTo(0,1000)
    Click Element    ${FACEBOOK ICON}
    ${FB_URL} =    Go To New Window    ${TRUE}
    Log    ${FB_URL}
    Should Be True    "${FACEBOOK URL}"=="${FB_URL}"
    Page Should Contain Element    //a[@aria-label="Musala Soft profile photo"]
    Click Element    //a[@aria-label="Musala Soft profile photo"]
    Sleep    ${DEFAULT_SLEEP}
    ${GOAL FB PIC URL} =    Set Variable    https://www.facebook.com/MusalaSoft/photos/a.152166551470703/3926723730681614/
    ${PIC URL} =    Get Location
    Should Be True    "${PIC URL}"=="${GOAL FB PIC URL}"

*** Keywords ***
Check For Popup
    Log    Checking for popup . . .
    ${is_there} =    Run Keyword and Return Status    Wait Until Element Is Visible    //div[@id="cookie-law-info-bar"]    ${LOAD_BREAK}
    Run Keyword If    ${is_there}    Close Popup
    Run Keyword Unless    ${is_there}    Log    DONE

Close Popup
    Click Element    //div[@class="cli-bar-btn_container"]/a
    Sleep    ${DEFAULT_SLEEP}
    Log    CLOSED

Go To New Window
    [Arguments]    ${RETURN}=${FALSE}
    ${WINDOWS} =    Get Window Handles
    ${WINDOW 1} =    Get From List    ${WINDOWS}    0
    ${WINDOW 2} =    Get From List    ${WINDOWS}    1
    Switch Window    ${WINDOW 2}
    Return From Keyword If    ${RETURN}==${FALSE}
    Sleep    ${DEFAULT_SLEEP}
    ${URL} =    Get Location
    Return From Keyword    ${URL}
