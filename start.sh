#!/bin/sh

echo "Izberete BROWSER: "  
echo "1: Chrome"
echo "2: Firefox"
echo "se ostanato: IZLEZ"
read n
case $n in
  1) echo "izbravte 1: Chrome" && SBROWSER="chrome_browser";; 
  2) echo "izbravte 2: Firefox" && SBROWSER="firefox_browser";; 
  *) echo "izlez" && exit;;
esac

SS_NOW=$(date +%Y-%m-%d-%H-%M-%S-%6N);
OUTPUTDIR=./results/log-$SBROWSER-$SS_NOW
mkdir -p OUTPUTDIR
echo "Se Startuva... Proverete logovi vo folder $OUTPUTDIR";

robot  --variablefile ./varijabli/$SBROWSER.yaml --variablefile ./varijabli/varijabli.yaml --skip "SKIP" --outputdir $OUTPUTDIR ./

